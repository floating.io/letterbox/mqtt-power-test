package main

import (
	"fmt"
	"os"
	"strings"
	
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

const (
	MQTT_SERVER string = "mqtt.gtu.floating.io"
	MESSAGE_TOPIC      = "test"
)

func main() {
	if (len(os.Args) < 2) {
		fmt.Println(`Need a command.  Try "get" or "send <message>".`)
		os.Exit(2);
	}

	options := mqtt.NewClientOptions()
	options.AddBroker(fmt.Sprintf("tcp://%s:1883", MQTT_SERVER))
	options.SetClientID("Test-Client")

	client := mqtt.NewClient(options)
	tok := client.Connect()
	tok.Wait();
	if tok.Error() != nil {
		fmt.Printf("ERROR: %s\n", tok.Error().Error())
		os.Exit(1)
	}

	switch os.Args[1] {
	case "send":
		message := strings.Join(os.Args[2:], " ")
		tok = client.Publish(MESSAGE_TOPIC, 0, true, message)
		tok.Wait();
		if tok.Error() != nil {
			fmt.Printf("ERROR: %s\n", tok.Error().Error())
			os.Exit(3)
		}
		fmt.Printf("sent [%s].\n", message);
		
	case "get":
		done := make(chan bool, 1)
		client.Subscribe(MESSAGE_TOPIC, 0, func(c mqtt.Client, m mqtt.Message) {
			fmt.Printf("Message: [%s]\n", m.Payload())

			done <- true
		})
		<- done
		os.Exit(0)

	default:
		fmt.Printf("Unknown command.  Try get or send <message>.\n");
		os.Exit(2)
	}

	os.Exit(0);
}
