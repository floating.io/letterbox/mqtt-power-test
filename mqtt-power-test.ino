// -*- c++ -*-
// Generic ESP8266 "Connect to MQTT and Grab a Message, then sleep" toy.
// Just for testing purposes in trying to get some power consumption
// numbers.
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Configuration.
//
// You'll have to set up your own config.h; this is not included in the
// repository because it will, by necessity, contain passwords.  To get
// started, copy config.template to config.h, and then edit it as you
// care to.  Should be easy enough...
#include "config.h"

WiFiClient wClient;
PubSubClient client(wClient);

void wifi_setup() {
  Serial.printf("wifi: connecting to %s ...\n", WIFI_SSID);
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.printf("wifi: ip address = %d.%d.%d.%d\n",
    WiFi.localIP()[0],
    WiFi.localIP()[1],
    WiFi.localIP()[2],
    WiFi.localIP()[3]);
}

void setup() {
  // NOTE: The odd bitrate was chosen because it appears to be
  //       the default startup value of the ESP8266.  That allows
  //       us to read its output without mucking with the speed.
  Serial.begin(74880);
  Serial.setTimeout(2000);
  while (!Serial) delay(1);
  delay(1000);
  Serial.println("Waking up and working...");

  wifi_setup();
  randomSeed(micros());

  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(my_callback);
}


void my_callback(char* topic, byte* payload, unsigned int length) {
  // display message
  char msg[128];
  strncpy(msg, (char *)payload, length);
  msg[length] = 0;
  
  Serial.printf("message: %s\n", msg);
  delay(1000);

  // go to sleep for one minute.
  Serial.println("Going to sleep.");
  delay(10);
  ESP.deepSleep(5 * 1000000);
}

void loop() {
  // Connect to MQTT and subscribe
  while (!client.connected()) {
    Serial.printf("mqtt: connecting to %s\n", MQTT_SERVER);
    String id("mqtt-power-test-");
    id += String(random(0xfffff), HEX);

    if (client.connect(id.c_str(), NULL, NULL)) {
      Serial.println("  - connected.");
      if (!client.subscribe(MESSAGE_TOPIC, 0)) {
        Serial.println("Uh, failed to subscribe for some reason.\n");
      }
    } else {
      Serial.printf("mqtt: connect failed: %d\n", client.state());
      Serial.println("retry in 5...");
      delay(5000);
    }
  }

  // wait for stuff to happen
  delay(10);
  client.loop();
}
