# mqtt-power-test

Test project for estimating power consumption for grabbing messages
from an MQTT server over WiFi every so often.

## test-client

This is a command-line Linux client, written in go, intended to send
simple messages to our test queue.  Nothing more or less.  This exists
just so that we can prep the queue and ensure that there's something
for the Feather to read.

## esp-feather

Code to run on an Adafruit ESP8266 HUZZAH Feather.  Or whatever they
call it.  Note that there is little to no attempt to actually drive a
display here; we just want to know how much juice it takes to keep
updated.  And also, how long it takes to connect to a WiFi network and
gather that data...
